from django.utils.translation import ugettext_lazy
try:
    from pretix.base.plugins import PluginConfig
except ImportError:
    raise RuntimeError("Please use pretix 2.7 or above to run this plugin!")


class PluginApp(PluginConfig):
    name = 'pretix_superscreen'
    verbose_name = 'The Pretix Superscreen Template plugin'

    class PretixPluginMeta:
        name = ugettext_lazy('The Pretix Superscreen Template plugin')
        author = 'Alexios Konsoulas'
        description = ugettext_lazy('Superscreen Template plugin with oberauer style for pretix ticket system')
        visible = True
        version = '1.1.8'
        compatibility = "pretix>=2.7.0"

    def ready(self):
        from . import email  # NOQA
        from . import signals  # NOQA


default_app_config = 'pretix_superscreen.PluginApp'
